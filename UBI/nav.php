<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container">
	      <a class="navbar-brand" href="index.html"><span>Square.</span></a>
	      <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> Menu
	      </button>

	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav nav ml-auto">
	          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="home"><span>หน้าแรก</span></a></li>
			  <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
				  เกี่ยวกับเรา
				</a>
				<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
				  <li><a class="dropdown-item" href="#">ทีมงานของเรา</a></li>
				  <li><a class="dropdown-item" href="#">ดาวน์โหลด</a></li>
				  <li><hr class="dropdown-divider"></li>
				 
				</ul>
			  </li>
	          <li class="nav-item dropdown">
				<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
				  ข่าวประชาสัมพันธ์
				</a>
				<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
				  <li><a class="dropdown-item" href="#">รูปภาพกิจกรรม/แกลอรี่</a></li>
				  
				</ul>
			  </li>
	          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="team"><span>กิจกรรม/โครงการ</span></a></li>
	          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="testimony"><span>บทความ</span></a></li>
	          <li class="nav-item"><a href="#" class="nav-link" data-nav-section="blog"><span>ผลิตภัณฑ์ภายใต้ศูนย์</span></a></li>
	          
	          <li class="nav-item cta"><a href="#" class="nav-link">Request a quote</a></li>

	        </ul>
	      </div>
	    </div>
	  </nav>
      <!DOCTYPE html>
<html lang="en">
  <head>
    <title>Square - Free Bootstrap 4 Template by Colorlib</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500,700,900" rel="stylesheet">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">
    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
  </head>