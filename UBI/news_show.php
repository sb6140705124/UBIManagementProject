<?php
require_once('header.php');
?>
<?php
  echo "=====>" ,filter_input(INPUT_GET,"news_id");
      $news_id = filter_input(INPUT_GET,"news_id"); //ส่งข้อมูล method GET ผ่าน Url จากการกดปุ่มแก้ไข
      $sql_news = "SELECT * FROM news WHERE news_id = $news_id";
      $result_news = $objCon->query($sql_news);
      $row_news = $result_news->fetch_assoc();
    ?>
 <section class="hero-wrap js-fullheight" style="background-image: url('images/41723662_885304298329015_346751457338327040_o.jpg');" data-section="home">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-start" data-scrollax-parent="true">
          <div class="col-md-6 ftco-animate mt-5" data-scrollax=" properties: { translateY: '70%' }">
          </div>
        </div>
      </div>
    </section>
<section class="ftco-section ftco-project bg-light" data-section="projects">
 <div class="col-md-12 heading-section text-center ftco-animate">
  <h2 class="mb-4">ข่าวประชาสัมพันธ์</h2>

<div class="container">
<div class="card mb-3" style="max-width: 100%;">
  <div class="row g-0">
    <div class="col-md-5">
      <img src="upload/pic/<?php echo $row_news["news_image"] ?>" class="img-fluid" alt="Colorlib Template">
    </div>
    <div class="col-md-7">
      <div class="card-body">
      
        <h5 class="card-title"><?php echo $row_news['news_title']; ?></h5>
        <p class="card-text"><?php echo $row_news['news_detail']; ?></p>
        <p class="card-text"><small class="text-muted"></h5>
        <p class="card-text"><?php echo $row_news['news_cre_date']; ?></small></p>
      </div>
    </div>
  </div>
</div>

</div>

 </div>
 </section>        
</section>



<?php
require_once('footer.php');
?>	