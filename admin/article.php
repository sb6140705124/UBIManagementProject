<?php
  include"header_admin.php";
  include"sidebar.php";
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper font">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>จัดการบทความ</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="admin_page.php">หน้าหลัก</a></li>
              <li class="breadcrumb-item active">รายการบทความ</li>
            </ol>
          </div>
        </div>
        <div class="row mb-2">
          <div class="col-sm-12">
                 <button type="button" class="btn btn-primary" onclick="location.href = 'ar_add.php';";> + เพิ่มบทความ </button>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title font">รายการบทความ</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
<?php
$q = "SELECT * FROM `article`";
$qq = $objCon->query($q);
$qn = $qq->num_rows;
// echo "มีข้อมูลพนักงานจำนวน $qn คน <br>";
?>
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 5%">
                        ลำดับ
                      </th>
                      <th style="width: 20%">
                        หัวข้อบทความ
                      </th>
                      <th style="width: 30%">
                        รายละเอียดอย่างย่อ
                      </th>
                      <th>
                        รูปภาพ
                      </th>
                      <th style="width: 10%" class="text-center">
                        วันที่สร้าง
                      </th>
                      <th style="width: 10%" class="text-center">
                        วันที่ปรับปรุง
                      </th>
                      <th style="width: 15%">
                      </th>
                  </tr>
              </thead>
              <tbody>
<?php
if($qn > 0){
  for($i = 0; $i<$qn; $i++){
    $row = $qq->fetch_assoc();
    $number = $i+1;
    //echo "ตำแหน่งงาน :".$row['posName']."<br>";
?>
                  <tr>
                      <td>
                        <?php echo $number; ?>
                      </td>
                      <td>
                          <a>
                          <?php echo $row['ar_title']; ?>
                          </a>
                      </td>
                      <td>
                        <?php echo $row['ar_preview']; ?>
                      </td>
                      <td><?php echo "<a href='../upload/pic/$row[ar_image]' target = '_blank'><img src='../upload/pic/$row[ar_image]' width='100'/></a>" ?></td>
                      <td><?php echo $row['ar_cre_date']; ?></td>
                      <td><?php echo $row['ar_up_date']; ?></td>
                      <td class="project-actions text-right">
                          <a class="btn btn-info btn-sm" href="ar_edit.php?ar_id=<?php echo $row['ar_id']; ?>">
                              <i class="fas fa-pencil-alt">
                              </i>
                              Edit
                          </a>
                          <a class="btn btn-danger btn-sm" href="article_cd.php?ar_id=<?php echo $row['ar_id']; ?>" onclick="return confirm('คุณต้องการลบข้อมูลที่เลือก')">
                              <i class="fas fa-trash">
                              </i>
                              Delete
                          </a>
                      </td>
                  </tr>
<?php }} ?>
              </tbody>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php
  include"footer_admin.php";
?>