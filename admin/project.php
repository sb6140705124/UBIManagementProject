<?php
 include"header_admin.php";
?>
<title>กิจกรรม/โครงการ</title>

<!-- Google Font: Source Sans Pro -->

<!-- Font Awesome -->
<link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="dist/css/adminlte.min.css">

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="css/fong.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:wght@300&display=swap" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
<!-- Navbar -->

  <!-- Left navbar links -->
 

  <!-- SEARCH FORM -->
  <form class="form-inline ml-3">
    <div class="input-group input-group-sm">
      <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
      <div class="input-group-append">
        <button class="btn btn-navbar" type="submit">
          <i class="fas fa-search"></i>
        </button>
      </div>
    </div>
  </form>

  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
 
   
  
    
    
  </ul>
</nav>
<!-- /.navbar -->

<!-- Main Sidebar Container -->
<? include"sidebar.php";?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">


  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    
    <div class="card font">
      <div class="card-header">
        <h3 class="card-title font">กิจกรรม/โครงการ</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
      <div class="container col-sm-15">
    <div class="mt-5 my-5"></div>
    <?php
    $q = "SELECT * FROM `project`";
    $qq = $objCon->query($q);
    $qn = $qq->num_rows;
    // echo "มีข้อมูลพนักงานจำนวน $qn คน <br>";
    ?>
    <div class="d-grid gap-2 d-md-flex justify-content-md-end">
    <button type="button" class="btn btn-success" onclick="location.href = 'project_add.php';";>เพิ่ม</button>
    </div>
    <!--ตาราง-->
    <table class="table table-hover">
  <thead>
    <tr>
      <th>เลขที่กิจกรรม/โครงการ</th>
      <th>ชื่อกิจกรรม/โครงการ</th>
      <th>รูปภาพ</th>
      <th>รายละเอียดกิจกรรม/โครงการ</th>
      <th>วันที่สร้าง</th>
      <th>วันที่ปรับปรุง</th>
      <th></th>
      <th></th>

    </tr>
  </thead>
  <tbody>
   
   <?php
     if($qn > 0){
         for($i = 0; $i<$qn; $i++){
             $row = $qq->fetch_assoc();
             //echo "ตำแหน่งงาน :".$row['posName']."<br>";
             ?>
          <tr>
      <td><?php echo $row['pj_id']; ?></td>
      <td><?php echo $row['pj_name']; ?></td>
      
      <td><?php echo "<a href='upload/$row[pj_image]' target = '_blank'><img src='upload/$row[pj_image]' width='100'/></a>" ?></td>
      <td><?php echo $row['pj_detail']; ?></td>
      <td><?php echo $row['pj_cre_date']; ?></td>
      <td><?php echo $row['pj_up_date']; ?></td>
      <td><a class="btn btn-outline-warning" href="project_edit.php?pj_id=<?php echo $row['pj_id']; ?>">แก้ไข</a></td>
      <td><a class="btn btn-outline-danger" href="project_del.php?pj_id=<?php echo $row['pj_id']; ?>" onclick="return confirm('คุณต้องการลบข้อมูลที่เลือก')">ลบ</a></td>
           
           
         </tr>
        <?php
         }
       ?>
         </tbody>
       </table>
    <?php
     } 
     ?>
      
        
        
    

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
