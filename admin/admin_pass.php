<?php
  include"header_admin.php";
  include"sidebar.php";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper font">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>จัดการรหัสผ่าน</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="admin.php">รายการรหัสผ่าน</a></li>
              <li class="breadcrumb-item active">แก้ไขรหัสผ่าน</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
    <form action="admin_cd.php" method="post" enctype="multipart/form-data">
    
      <div class="row">
        <div class="col-md-9">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">แก้ไขรหัสผ่าน</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputDescription">รหัสผ่านเดิม</label>
                <input type="password" class="form-control"  name="admin_passold" id="admin_passold" placeholder="" >
              </div>
            
              <div class="form-group">
                <label for="inputDescription">รหัสผ่านใหม่</label>
                <input type="password" class="form-control"  name="admin_passnew" id="admin_passnew" placeholder="" >
              </div>
            
              <div class="form-group">
                <label for="inputDescription">ยืนยันรหัสผ่านใหม่</label>
                <input type="password" class="form-control"  name="admin_passrenew" id="admin_passrenew" placeholder="" >
              </div>
              
              
              </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <button type="submit"name="btneditpass" value="1" class="btn btn-info"> แก้ไขรหัสผ่าน</button>
          <button type="button" onclick=window.history.back() class="btn btn-danger">ยกเลิก</button>
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  include"footer_admin.php";
?>











