<?php
  include"header_admin.php";
  ?>
  <title>แก้ไขข่าวประชาสัมพันธ์</title>

  <!-- Google Font: Source Sans Pro -->
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="css/fong.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:wght@300&display=swap" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
 
    <!-- Left navbar links -->
   

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
   </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php require_once"sidebar.php";?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title font">เพิ่มข่าวประชาสัมพันธ์</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        <div class="container col-sm-9">
      <div class="mt-5 my-5"></div>
      

      <form action="news_cd.php" method="post" enctype="multipart/form-data">
     <div class="font">
<div class="mt-5 my-5"></div>   
       
       
        <div class="form-group row">
          <label for="news_id" class="col-sm-2 col-form-label"> เลขที่ข่าวประชาสัมพันธ์:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="news_id" id="news_id" placeholder="" value=""readonly>
          </div>
        </div>

        <div class="form-group row">
          <label for="news_title" class="col-sm-2 col-form-label"> หัวข้อข่าวประชาสัมพันธ์:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="news_title" id="news_title" placeholder="" value="">
          </div>
        </div>

        <div class="form-group row">
          <label for="news_detail" class="col-sm-2  col-form-label">รายละเอียดข่าว:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="news_detail" id="news_detail" placeholder="" value="">
          </div>
        </div>

        <div class="form-group row">
          <label for="news_image" class="col-sm-2  col-form-label">รูปภาพ:</label>
          <div class="col-sm-10">

          <div class="mb-3">
          <label for="news_image" class="form-label"> </label>
          <input class="form-control" type="file" id="news_image"name="news_image">
          </div>
          </div>
          </div>
        

        <div class="form-group row">
          <label for="news_cre_date" class="col-sm-2  col-form-label">วันที่สร้าง:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="news_cre_date" id="news_cre_date" placeholder="" value="">
          </div>
        </div>

        <div class="form-group row ">
          <label for="news_up_date" class="col-sm-2 col-form-label">วันที่ปรับปรุง :</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="news_up_date" id="news_up_date" placeholder="" value="">
          </div>
        </div>

        
        
        


        <hr>
        &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; <button type="submit"name="btnins" value="1" class="btn btn-success"> เพิ่ม</button>
        &emsp; &emsp; &emsp; <button type="button" onclick=window.history.back() class="btn btn-danger">ยกเลิก</button>
      </form>
    </div>
        </div>
        <!-- /.card-body -->
       
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>