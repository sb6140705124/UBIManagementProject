<?php
  include"header_admin.php";
  include"sidebar.php";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper font">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>จัดการข่าวประชาสัมพันธ์</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="news.php">รายการข่าวประชาสัมพันธ์</a></li>
              <li class="breadcrumb-item active">แก้ไขข่าวประชาสัมพันธ์</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php
      $news_id = filter_input(INPUT_GET,"news_id"); //ส่งข้อมูล method GET ผ่าน Url จากการกดปุ่มแก้ไข
      $sql_news = "SELECT * FROM news WHERE news_id = $news_id";
      $result_news = $objCon->query($sql_news);
      $row_news = $result_news->fetch_assoc();
      
    ?>

    <!-- Main content -->
    <section class="content">
    <form action="news_cd.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="hidden_news_id" id="hidden_news_id" value="<?php echo $news_id; ?>"> 
      <div class="row">
        <div class="col-md-9">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">แก้ไขข่าวประชาสัมพันธ์</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">หัวข้อข่าวประชาสัมพันธ์</label>
                <input type="text" class="form-control"  name="news_title" id="news_title" placeholder="" value="<?php echo $row_news['news_title']; ?>">
              </div>
              <div class="form-group">
                <label for="inputDescription">รายละเอียดอย่างย่อ</label>
                <input type="text" class="form-control"  name="news_preview" id="news_preview" placeholder="" value="<?php echo $row_news['news_preview']; ?>">
              </div>
              <div class="form-group">
                <label for="inputClientCompany">รายละเอียดข่าว</label>
                <textarea id="inputDescription" class="form-control" name="news_detail" id="news_detail" rows="4"> <?php echo $row_news['news_detail']; ?> </textarea>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">รูปภาพ</label>
                <input class="form-control" type="file" id="news_image"name="news_image">
                        <?php 
                          if ($row_news['news_image'] != '') {
                            echo "<input type='checkbox' name='Delete_old_pic' value='Yes'>ลบไฟล์รูปภาพ<br>";
                            echo "$row_news[news_image]<br><br>";
                            echo "<img src='../upload/pic/$row_news[news_image]' width='200'/>";
                            echo "<input type='hidden' name='hidden_news_pic' id='hidden_news_pic' value='$row_news[news_image]'>";
                          } else {
                            echo '<font color=red>ไม่มีข้อมูล</font>';
                        }?>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <button type="submit"name="btnedit" value="1" class="btn btn-info"> แก้ไข</button>
          <button type="button" onclick=window.history.back() class="btn btn-danger">ยกเลิก</button>
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  include"footer_admin.php";
?>











