<?php 
session_start();
require_once('config.php'); 

$btnins = $_POST['btnins'];
$btnedit = $_POST['btnedit'];
$ar_id = filter_input(INPUT_GET,"ar_id");

if($btnins){
    $ar_id = $_POST['ar_id'];
    $ar_title = $_POST['ar_title'];
    $ar_preview = $_POST['ar_preview'];
    $ar_detail = $_POST['ar_detail'];
    $datenow = date('Y-m-d H:i:s');
    
    @$file = $_FILES['ar_image']['tmp_name'];
    @$file_name = $_FILES['ar_image']['name'];
    @$file_size = $_FILES['ar_image']['size'];

    $now = date('Ymdgis'); //สร้างชื่อไฟล์รูปใหม่ให้ไม่ซ้ำกัน ด้วยค่าวันเวลา

    //ตรวจสอบว่าถ้ามีการเพิ่มรูปเข้ามา ให้สร้างชื่อไฟล์รูปภาพเตรียมไว้ || แต่ถ้าไม่มีการเพิ่มรูป ชื่อไฟล์รูปภาพจะเท่ากับค่าว่าง
    if (!empty($_FILES['ar_image']['tmp_name'])) {
        $rest = strrchr($_FILES['ar_image']['name'], '.');
        $File_name = $now.$rest;
        $path = '../upload/pic/'.$File_name;
    } else {
        $File_name = '';
    }
    $q = "INSERT INTO `article` (`ar_id`, `ar_title`, `ar_preview`, `ar_detail`, `ar_image`, `ar_cre_date`, `ar_up_date`,`admin_id`) VALUES (NULL, '$ar_title', '$ar_preview','$ar_detail', '$File_name', '$datenow', '$datenow','$_SESSION[admin_id]')";

    $qq = $objCon->query($q);
    if($qq){
    if (!empty($_FILES['ar_image']['name'])) {
        if (!move_uploaded_file($_FILES['ar_image']['tmp_name'], $path)) {
            echo 'Upload Error';
        }
    }
    echo "<script>alert('คุณได้เพิ่มบทความแล้ว!');</script>";
    echo "<script>window.location.href='article.php'</script>";
    }else{
    echo "<script>alert('กรุณาลองใหม่อีกครั้ง.');</script>";
    echo "<script>window.location.href='article.php'</script>";
    }

}elseif($btnedit){
    $ar_id = $_POST['hidden_ar_id'];
    $ar_title = $_POST['ar_title'];
    $ar_preview = $_POST['ar_preview'];
    $ar_detail = $_POST['ar_detail'];
    $datenow = date('Y-m-d H:i:s');
    
    @$old_ar_pic = filter_input(INPUT_POST,"hidden_ar_pic");

    @$file = $_FILES['ar_image']['tmp_name'];
    @$file_name = $_FILES['ar_image']['name'];
    @$file_size = $_FILES['ar_image']['size'];

    $now = date('Ymdgis'); //สร้างชื่อไฟล์รูปใหม่ให้ไม่ซ้ำกัน ด้วยค่าวันเวลา

    //ตรวจสอบว่า ถ้ามีการแก้ไขเพิ่มรูปใหม่เข้ามา ให้สร้างชื่อไฟล์รูปภาพเตรียมไว้ || แต่ถ้าติ๊กที่ลบข้อมูล รูปจะถูกลบออกจากโฟลเดอร์ pic และชื่อไฟล์รูปภาพจะเท่ากับค่าว่าง || แต่ถ้าไม่แก้ไขรูปภาพ ชื่อรูปจะเหมือนเดิม
    if (!empty($_FILES['ar_image']['tmp_name'])) {
        $rest = strrchr($_FILES['ar_image']['name'], '.'); //นามสกุลไฟล์
        $File_name = $now.$rest; //ชื่อไฟล์+นามสกุล
        $path = '../upload/pic/'.$File_name; //สร้าง path เพื่อเก็บรูปภาพ
    } elseif (@$_POST['Delete_old_pic'] == 'Yes') {
        @unlink('../upload/pic/'.$old_ar_pic);
        $File_name = ''; //ชื่อไฟล์ว่างเปล่า
    } else {
        $File_name = $old_ar_pic; //ชื่อไฟล์เดิม
    }

    $q = "UPDATE `article` SET `ar_title` = '$ar_title', `ar_preview` = '$ar_preview', `ar_detail` = '$ar_detail', `ar_image` = '$File_name', `ar_up_date` = '$datenow' WHERE `article`.`ar_id` = $ar_id";
    
    $qq = $objCon->query($q);
    if($qq){
        if (!empty($_FILES['ar_image']['name'])) {
            if (!empty($old_ar_pic)) {
                @unlink('../upload/pic/'.$old_ar_pic); //ลบรูปเดิมทิ้ง
            }
            if (!move_uploaded_file($_FILES['ar_image']['tmp_name'], $path)) {
                echo 'Upload Error';
            } //อัปโหลดไฟล์และทำการตรวจสอบการอัปโหลด
        }
        echo "<script>alert('คุณได้แก้ไขบทความแล้ว!');</script>";
        echo "<script>window.location.href='article.php'</script>";
    }else{
        echo "<script>alert('กรุณาลองใหม่อีกครั้ง.');</script>";
        echo "<script>window.location.href='article.php'</script>";
    }
}if($ar_id) { //ลบข้อมูล
    //ส่งค่า product_id แบบ method GET ผ่าน Url
    $ar_id = filter_input(INPUT_GET,"ar_id");

    //ดึงข้อมูลที่ต้องการลบขึ้นมา เพื่อให้ได้ชื่อรูปภาพ
    $sql_article = "SELECT * FROM article WHERE ar_id = $ar_id";
    $result_article = $objCon->query($sql_article);
    $row_article = $result_article->fetch_assoc();

    $ar_image = $row_article['ar_image'];

    //คำสั่ง sql ในการลบข้อมูล
    $q = "DELETE FROM `article` WHERE `article`.`ar_id` = $ar_id";
        $qq = $objCon->query($q);
        if($qq){

    //ตรวจสอบว่าลบข้อมูลได้หรือไม่ ถ้าลบได้ให้ลบรูปภาพออกจากโฟลเดอร์ pic ด้วย
    if ($qq) {
        @unlink('../upload/pic/'.$ar_image);
        echo "<script langquage='javascript'>
				alert('ลบข้อมูลเรียบร้อยแล้ว')
				window.location='article.php';
			</script>";
    } else {
        echo "<script langquage='javascript'>
				alert('ไม่สามารถลบข้อมูลได้')
				window.location='article.php';
			</script>";
        }
    }
}
?>

