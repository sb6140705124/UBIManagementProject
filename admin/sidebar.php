  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="admin_page.php" class="brand-link">
    <img src="img/46444397_918396391686472_5318882792082243584_n.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light font">ศูนย์บ่มเพาะวิสาหกิจ</span>
    </a>
    <link href="/css/fong.css" rel="stylesheet">

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex font">
       
        <?php if(isset($_SESSION['admin_id'])) { ?>                    
        <div class="info">
        <a href="#" class="d-block"><?php echo $_SESSION["admin_fname"];?> <?php echo $_SESSION["admin_lname"];?></a>
      
      </div>
      <a  class="btn btn-danger" href="logout.php">logout</a>
            <?php } else { ?>
                    <div class="btn-group" role="group" aria-label="Basic mixed styles example">
                    <a  class="btn btn-primary" href="login.php">Login</a>
           
            <?php } ?>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline font">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item font">
            <a href="" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ข้อมูลเจ้าหน้าที่
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="admin_edit.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>แก้ไขข้อมูลส่วนตัว</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="admin_pass.php" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>แก้ไขรหัสผ่าน</p>
                </a>
              </li>
              </ul>
          </li>

          <li class="nav-item font">
            <a href="news.php" class="nav-link">
              <i class="nav-icon fas fa-th"></i>
              <p>
                ข่าวประชาสัมพันธ์
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>

          <li class="nav-item font">
            <a href="article.php" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
              <p>
                บทความ
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>

          <li class="nav-item font">
            <a href="project.php" class="nav-link">
            <i class="nav-icon fas fa-chart-pie"></i>
              <p>
                กิจกรรม/โครงการ
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>

          <li class="nav-item font">
            <a href="product.php" class="nav-link">
            <i class="nav-icon fas fa-tree"></i>
              <p>
                ผลิตภัณฑ์ภายใต้การบ่มเพาะ
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>

          <li class="nav-item font">
            <a href="entre.php" class="nav-link">
            <i class="nav-icon fas fa-edit"></i>
              <p>
                ข้อมูลผู้ประกอบการ
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>

          <li class="nav-item font">
            <a href="type.php" class="nav-link">
            <i class="nav-icon fas fa-table"></i>
              <p>
                ประเภทของผู้ประกอบการ
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>

          <li class="nav-item font">
            <a href="file.php" class="nav-link">
            <i class="nav-icon far fa-calendar-alt"></i>
              <p>
                ไฟล์ดาวน์โหลด
                <span class="right badge badge-danger"></span>
              </p>
            </a>
          </li>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>