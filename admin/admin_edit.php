<?php
  include"header_admin.php";
  include"sidebar.php";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper font">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>จัดการข้อมูลส่วนตัว</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="admin.php">รายการข้อมูลส่วนตัว</a></li>
              <li class="breadcrumb-item active">แก้ไขข้อมูลส่วนตัว</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php
      //$admin_id = filter_input(INPUT_GET,"admin_id"); //ส่งข้อมูล method GET ผ่าน Url จากการกดปุ่มแก้ไข
      $sql_admin = "SELECT * FROM admin WHERE admin_id = '$_SESSION[admin_id]'";
      $result_admin = $objCon->query($sql_admin);
      $row_admin = $result_admin->fetch_assoc();
      
    ?>

    <!-- Main content -->
    <section class="content">
    <form action="admin_cd.php" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="col-md-9">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">แก้ไขข้อมูลส่วนตัว</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputDescription">ชื่อ</label>
                <input type="text" class="form-control"  name="admin_fname" id="admin_fname" placeholder="" value="<?php echo $row_admin['admin_fname']; ?>">
              </div>
              <div class="form-group">
                <label for="inputDescription">นามสกุล</label>
                <input type="text" class="form-control"  name="admin_lname" id="admin_lname" placeholder="" value="<?php echo $row_admin['admin_lname']; ?>">
              </div>
              <div class="form-group">
                <label for="inputDescription">อีเมล์</label>
                <input type="text" class="form-control"  name="admin_email" id="admin_email" placeholder="" value="<?php echo $row_admin['admin_email']; ?>">
              </div>
              <div class="form-group">
                <label for="inputDescription">เบอร์โทรศัพท์</label>
                <input type="text" class="form-control"  name="admin_phone" id="admin_phone" placeholder="" value="<?php echo $row_admin['admin_phone']; ?>">
              </div>
              
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <button type="submit"name="btnedit" value="1" class="btn btn-info"> แก้ไข</button>
          <button type="button" onclick=window.history.back() class="btn btn-danger">ยกเลิก</button>
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  include"footer_admin.php";
?>











