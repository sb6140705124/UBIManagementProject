<?php
  include"header_admin.php";
  include"sidebar.php";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper font">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>จัดการบทความ</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="article.php">รายการบทความ</a></li>
              <li class="breadcrumb-item active">แก้ไขบทความ</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <?php
      $ar_id = filter_input(INPUT_GET,"ar_id"); //ส่งข้อมูล method GET ผ่าน Url จากการกดปุ่มแก้ไข
      $sql_article = "SELECT * FROM article WHERE ar_id = $ar_id";
      $result_article = $objCon->query($sql_article);
      $row_article = $result_article->fetch_assoc();
      
    ?>

    <!-- Main content -->
    <section class="content">
    <form action="article_cd.php" method="post" enctype="multipart/form-data">
    <input type="hidden" name="hidden_ar_id" id="hidden_ar_id" value="<?php echo $ar_id; ?>"> 
      <div class="row">
        <div class="col-md-9">
          <div class="card card-info">
            <div class="card-header">
              <h3 class="card-title">แก้ไขบทความ</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">หัวข้อบทความ</label>
                <input type="text" class="form-control"  name="ar_title" id="ar_title" placeholder="" value="<?php echo $row_article['ar_title']; ?>">
              </div>
              <div class="form-group">
                <label for="inputDescription">รายละเอียดอย่างย่อ</label>
                <input type="text" class="form-control"  name="ar_preview" id="ar_preview" placeholder="" value="<?php echo $row_article['ar_preview']; ?>">
              </div>
              <div class="form-group">
                <label for="inputClientCompany">รายละเอียดบทความ</label>
                <textarea id="inputDescription" class="form-control" name="ar_detail" id="ar_detail" rows="4"> <?php echo $row_article['ar_detail']; ?> </textarea>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">รูปภาพ</label>
                <input class="form-control" type="file" id="ar_image"name="ar_image">
                        <?php 
                          if ($row_article['ar_image'] != '') {
                            echo "<input type='checkbox' name='Delete_old_pic' value='Yes'>ลบไฟล์รูปภาพ<br>";
                            echo "$row_article[ar_image]<br><br>";
                            echo "<img src='../upload/pic/$row_article[ar_image]' width='200'/>";
                            echo "<input type='hidden' name='hidden_ar_pic' id='hidden_ar_pic' value='$row_article[ar_image]'>";
                          } else {
                            echo '<font color=red>ไม่มีข้อมูล</font>';
                        }?>
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <button type="submit"name="btnedit" value="1" class="btn btn-info"> แก้ไข</button>
          <button type="button" onclick=window.history.back() class="btn btn-danger">ยกเลิก</button>
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  include"footer_admin.php";
?>











