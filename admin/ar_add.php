<?php
  include"header_admin.php";
  include"sidebar.php";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper font">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>จัดการบทความ</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="article.php">รายการบทความ</a></li>
              <li class="breadcrumb-item active">เพิ่มบทความ</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
    <form action="article_cd.php" method="post" enctype="multipart/form-data">
      <div class="row">
        <div class="col-md-9">
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">เพิ่มบทความ</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                  <i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body">
              <div class="form-group">
                <label for="inputName">หัวข้อบทความ</label>
                <input type="text" class="form-control"  name="ar_title" id="ar_title" placeholder="" value="">
              </div>
              <div class="form-group">
                <label for="inputDescription">รายละเอียดอย่างย่อ</label>
                <input type="text" class="form-control"  name="ar_preview" id="ar_preview" placeholder="" value="">
              </div>
              <div class="form-group">
                <label for="inputClientCompany">รายละเอียดข่าว</label>
                <textarea id="inputDescription" class="form-control" name="ar_detail" id="ar_detail" rows="4"></textarea>
              </div>
              <div class="form-group">
                <label for="inputProjectLeader">รูปภาพ</label>
                <input class="form-control" type="file" id="ar_image"name="ar_image">
              </div>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <button type="submit"name="btnins" value="1" class="btn btn-primary"> เพิ่ม</button>
          <button type="button" onclick=window.history.back() class="btn btn-danger">ยกเลิก</button>
        </div>
      </div>
      </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php
  include"footer_admin.php";
?>











