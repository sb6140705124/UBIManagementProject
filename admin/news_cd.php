<?php 
session_start();
require_once('config.php'); 

$btnins = $_POST['btnins'];
$btnedit = $_POST['btnedit'];
$news_id = filter_input(INPUT_GET,"news_id");

if($btnins){
    $news_id = $_POST['news_id'];
    $news_title = $_POST['news_title'];
    $news_preview = $_POST['news_preview'];
    $news_detail = $_POST['news_detail'];
    $datenow = date('Y-m-d H:i:s');
    
    @$file = $_FILES['news_image']['tmp_name'];
    @$file_name = $_FILES['news_image']['name'];
    @$file_size = $_FILES['news_image']['size'];

    $now = date('Ymdgis'); //สร้างชื่อไฟล์รูปใหม่ให้ไม่ซ้ำกัน ด้วยค่าวันเวลา

    //ตรวจสอบว่าถ้ามีการเพิ่มรูปเข้ามา ให้สร้างชื่อไฟล์รูปภาพเตรียมไว้ || แต่ถ้าไม่มีการเพิ่มรูป ชื่อไฟล์รูปภาพจะเท่ากับค่าว่าง
    if (!empty($_FILES['news_image']['tmp_name'])) {
        $rest = strrchr($_FILES['news_image']['name'], '.');
        $File_name = $now.$rest;
        $path = '../upload/pic/'.$File_name;
    } else {
        $File_name = '';
    }
    $q = "INSERT INTO `news` (`news_id`, `news_title`, `news_preview`, `news_detail`, `news_image`, `news_cre_date`, `news_up_date`,`admin_id`) VALUES (NULL, '$news_title', '$news_preview','$news_detail', '$File_name', '$datenow', '$datenow','$_SESSION[admin_id]')";

    $qq = $objCon->query($q);
    if($qq){
    if (!empty($_FILES['news_image']['name'])) {
        if (!move_uploaded_file($_FILES['news_image']['tmp_name'], $path)) {
            echo 'Upload Error';
        }
    }
    echo "<script>alert('คุณได้เพิ่มข่าวแล้ว!');</script>";
    echo "<script>window.location.href='news.php'</script>";
    }else{
    echo "<script>alert('กรุณาลองใหม่อีกครั้ง.');</script>";
    echo "<script>window.location.href='news.php'</script>";
    }

}elseif($btnedit){
    $news_id = $_POST['hidden_news_id'];
    $news_title = $_POST['news_title'];
    $news_preview = $_POST['news_preview'];
    $news_detail = $_POST['news_detail'];
    $datenow = date('Y-m-d H:i:s');
    
    @$old_news_pic = filter_input(INPUT_POST,"hidden_news_pic");

    @$file = $_FILES['news_image']['tmp_name'];
    @$file_name = $_FILES['news_image']['name'];
    @$file_size = $_FILES['news_image']['size'];

    $now = date('Ymdgis'); //สร้างชื่อไฟล์รูปใหม่ให้ไม่ซ้ำกัน ด้วยค่าวันเวลา

    //ตรวจสอบว่า ถ้ามีการแก้ไขเพิ่มรูปใหม่เข้ามา ให้สร้างชื่อไฟล์รูปภาพเตรียมไว้ || แต่ถ้าติ๊กที่ลบข้อมูล รูปจะถูกลบออกจากโฟลเดอร์ pic และชื่อไฟล์รูปภาพจะเท่ากับค่าว่าง || แต่ถ้าไม่แก้ไขรูปภาพ ชื่อรูปจะเหมือนเดิม
    if (!empty($_FILES['news_image']['tmp_name'])) {
        $rest = strrchr($_FILES['news_image']['name'], '.'); //นามสกุลไฟล์
        $File_name = $now.$rest; //ชื่อไฟล์+นามสกุล
        $path = '../upload/pic/'.$File_name; //สร้าง path เพื่อเก็บรูปภาพ
    } elseif (@$_POST['Delete_old_pic'] == 'Yes') {
        @unlink('../upload/pic/'.$old_news_pic);
        $File_name = ''; //ชื่อไฟล์ว่างเปล่า
    } else {
        $File_name = $old_news_pic; //ชื่อไฟล์เดิม
    }

    $q = "UPDATE `news` SET `news_title` = '$news_title', `news_preview` = '$news_preview', `news_detail` = '$news_detail', `news_image` = '$File_name', `news_up_date` = '$datenow' WHERE `news`.`news_id` = $news_id";
    
    $qq = $objCon->query($q);
    if($qq){
        if (!empty($_FILES['news_image']['name'])) {
            if (!empty($old_product_pic)) {
                @unlink('../upload/pic/'.$old_product_pic); //ลบรูปเดิมทิ้ง
            }
            if (!move_uploaded_file($_FILES['news_image']['tmp_name'], $path)) {
                echo 'Upload Error';
            } //อัปโหลดไฟล์และทำการตรวจสอบการอัปโหลด
        }
        echo "<script>alert('คุณได้แก้ไขข่าวแล้ว!');</script>";
        echo "<script>window.location.href='news.php'</script>";
    }else{
        echo "<script>alert('กรุณาลองใหม่อีกครั้ง.');</script>";
        echo "<script>window.location.href='news.php'</script>";
    }
}if($news_id) { //ลบข้อมูล
    //ส่งค่า product_id แบบ method GET ผ่าน Url
    $news_id = filter_input(INPUT_GET,"news_id");

    //ดึงข้อมูลที่ต้องการลบขึ้นมา เพื่อให้ได้ชื่อรูปภาพ
    $sql_news = "SELECT * FROM news WHERE news_id = $news_id";
    $result_news = $objCon->query($sql_news);
    $row_news = $result_news->fetch_assoc();

    $news_image = $row_news['news_image'];

    //คำสั่ง sql ในการลบข้อมูล
    $q = "DELETE FROM `news` WHERE `news`.`news_id` = $news_id";
        $qq = $objCon->query($q);
        if($qq){

    //ตรวจสอบว่าลบข้อมูลได้หรือไม่ ถ้าลบได้ให้ลบรูปภาพออกจากโฟลเดอร์ pic ด้วย
    if ($qq) {
        @unlink('../upload/pic/'.$news_image);
        echo "<script langquage='javascript'>
				alert('ลบข้อมูลเรียบร้อยแล้ว')                       
				window.location='news.php';
			</script>";
    } else {
        echo "<script langquage='javascript'>
				alert('ไม่สามารถลบข้อมูลได้')
				window.location='news.php';
			</script>";
        }
    }
}
?>

