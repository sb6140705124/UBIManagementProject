<?php
session_start();
if(isset($_SESSION['admin_id'])){
	if($_SESSION["admin_status"] == "a"){
		header("location:admin_page.php");
	}else{
		header("location:user_page.php");
	}
} 
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.79.0">
    <title>ระบบ</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">

    

    <!-- Bootstrap core CSS -->
<link href="assets/dist/css/bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
  </head>
  <body class="text-center">
    
<main class="form-signin">
  <form name="form1" method="post" action="check_login.php">
    <img class="mb-4" src="img/46444397_918396391686472_5318882792082243584_n.jpg" width="100px">
    <h1 class="h3 mb-3 fw-normal">Please sign in</h1>
    <label for="inputEmail" class="visually-hidden">Email address</label>
    <input type="text" name="txtUsername" id="txtUsername" class="form-control" placeholder="โปรดกรอกชื่อเข้าระบบ" required autofocus>
    <label for="inputPassword" class="visually-hidden">Password</label>
    <input type="password" name="txtPassword" id="txtPassword" class="form-control" placeholder="กรุณากรอกรหัสผ่าน" required>

    <button class="w-100 btn btn-lg btn-primary" type="submit">เข้าสู่ระบบ</button>
    <p class="mt-5 mb-3 text-muted">&copy; Business Computer of Digital</p>
  </form>
</main>


    
  </body>
</html>
