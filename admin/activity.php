<?php
  session_start();
  include"config.php";
	if($_SESSION['admin_id'] == "")
	{
		echo "Please Login!";
		exit();
	}

	if($_SESSION['admin_status'] != "a")
	{

		echo "This page for Admin only!";
		exit();
	}	
	

	$strSQL = "SELECT * FROM admin WHERE admin_id = '".$_SESSION['admin_id']."' ";
	$objQuery = mysqli_query($objCon,$strSQL);
	$objResult = mysqli_fetch_array($objQuery,MYSQLI_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>กิจกรรมโครงการ</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link href="css/fong.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:wght@300&display=swap" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    
    <!-- SEARCH FORM -->
   

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">  
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <? include"sidebar.php";?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card font">
        <div class="card-header">
          <h3 class="card-title font">กิจกรรมโครงการ</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        <div class="container col-sm-9">
      <div class="mt-5 my-5"></div>
      <form>
      <tr>
      <td align="right">วันที่ :</td>  
      <td><? echo"$dateday";?></td>
    </tr>
          <div class="mt-5 my-5"></div>   
        <div class="form-group row">
          <label for="colFormLabel" class="col-sm-2  col-form-label">เลขที่กิจกรรมโครงการ:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="">
          </div>
        </div>
       
        <div class="form-group row">
          <label for="colFormLabel" class="col-sm-2 col-form-label">ชื่อกิจกรรมโครงการ:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="">
          </div>
        </div>
        <div class="form-group row">
          <label for="colFormLabel" class="col-sm-2  col-form-label">หัวข้อกิจกรรมโครงการ:</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" placeholder="">
          </div>
        </div>
      
        <div class="form-group row ">
          <label for="colFormLabel" class="col-sm-2 col-form-label">รายละเอียด:</label>
          <div class="col-sm-10">
              <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
          </div>
        </div>
      
        <hr>
        &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &ensp;  <button type="reset" class="btn btn-dark">ยกเลิก</button>
        &emsp; &emsp; &emsp; &emsp; &emsp; <button type="submit" class="btn btn-danger">ส่ง</button>
      </form>
    </div>
        </div>
        <!-- /.card-body -->
       
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

 
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
