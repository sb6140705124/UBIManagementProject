<?php
  include"header_admin.php";
  ?>
  <title>แก้ไขกิจกรรม/โครงการ</title>

  <!-- Google Font: Source Sans Pro -->
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="css/fong.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Bai+Jamjuree:wght@300&display=swap" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
 
    <!-- Left navbar links -->
   

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
   </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?php require_once"sidebar.php";?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">


    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      
      <div class="card ">
        <div class="card-header">
          <h3 class="card-title font">แก้ไขกิจกรรม/โครงการ</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        <div class="container col-sm-9">
      <div class="mt-5 my-5"></div>
        <?php
        $btnedit = filter_input(INPUT_GET,"btnedit");
        if($btnedit==false){
        $pj_id = filter_input(INPUT_GET,"pj_id");
        $q = "SELECT * FROM `project` WHERE pj_id = $pj_id";
        $qq = $objCon->query($q);
        $row = $qq->fetch_assoc();
        ?>

      <form action="project_save_edit.php" method="post" enctype="multipart/form-data">
     <div class="font">
<div class="mt-5 my-5"></div>   
       
       
        <div class="form-group row">
          <label for="pj_id" class="col-sm-2 col-form-label"> เลขที่กิจกรรม/โครงการ:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="pj_id" id="pj_id" placeholder="" value="<?php echo $row['pj_id']; ?>"readonly>
          </div>
        </div>

        <div class="form-group row">
          <label for="pj_name" class="col-sm-2 col-form-label"> ชื่อกิจกรรม/โครงการ:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="pj_name" id="pj_name" placeholder="" value="<?php echo $row['pj_name']; ?>">
          </div>
        </div>

        <div class="form-group row">
          <label for="pj_image" class="col-sm-2  col-form-label">รูปภาพ:</label>
          <div class="col-sm-10">
          <input type="file" class="form-control"  name="pj_image" id="pj_image" placeholder="" value="<?php echo $row['pj_image']; ?>">
          <?php 
                        if ($row['pj_image'] != '') {
                            echo "<input type='checkbox' name='Delete_old_pic' value='Yes'>ลบไฟล์รูปภาพ<br>";
                            echo "$row[pj_image]<br><br>";
                            echo "<img src='upload/$row[pj_image]' width='200'/>";
                            echo "<input type='hidden' name='hidden_product_pic' id='hidden_product_pic' value='$row[pj_image]'>";
                        } else {
                            echo '<font color=red>ไม่มีข้อมูล</font>';
                        }?>
          </div>

        <div class="form-group row">
          <label for="pj_detail" class="col-sm-2  col-form-label">รายละเอียดกิจกรรม/โครงการ:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="pj_detail" id="pj_detail" placeholder="" value="<?php echo $row['pj_detail']; ?>">
          </div>
        </div>
        </div>

        <div class="form-group row">
          <label for="pj_cre_date" class="col-sm-2  col-form-label">วันที่สร้าง:</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="pj_cre_date" id="pj_cre_date" placeholder="" value="<?php echo $row['pj_cre_date']; ?>">
          </div>
        </div>

        <div class="form-group row ">
          <label for="pj_up_date" class="col-sm-2 col-form-label">วันที่ปรับปรุง :</label>
          <div class="col-sm-10">
          <input type="text" class="form-control"  name="pj_up_date" id="pj_up_date" placeholder="" value="<?php echo $row['pj_up_date']; ?>">
          </div>
        </div>

        <hr>
        &emsp; &emsp; &emsp; &emsp; &emsp;&emsp; &emsp; &emsp; <button type="submit"name="btnedit" value="1" class="btn btn-success"> เพิ่ม</button>
        &emsp; &emsp; &emsp; <button type="reset" class="btn btn-danger">ยกเลิก</button>
      </form>
      <?php } ?>
    </div>
        </div>
        <!-- /.card-body -->
       
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  </div>
  

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>